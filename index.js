const kochiButton = document.getElementById('kochi')
const trivandrumButton = document.getElementById('trivandrum')
const citySection = document.getElementById('citySection')

kochiButton.addEventListener('click', () => {
    // 1. get temperature from API
    getTemperature(9.9312, 76.2673, "Kochi")
    // 2. display card
})

trivandrumButton.addEventListener('click', () => {
    // 1. get temperature from API
    getTemperature(8.5241, 76.9366, "Trivandrum")
    // 2. display card
})


function getTemperature(latitude, longitude, city) {
    fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            const temperature = data.current.temperature_2m
            const windSpeed = data.current.wind_speed_10m
            displayCard(temperature, windSpeed, city)
        })
        .catch(error => console.log(error))
}

function displayCard(temperature, windSpeed, city) {
    let image;
    if (city === "Trivandrum") {
        image = "https://www.holidify.com/images/bgImages/VARKALA.jpg"
    }
    else {
        image = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Kochi%2C_Fishing_nets_at_sunset%2C_Kerala%2C_India.jpg/375px-Kochi%2C_Fishing_nets_at_sunset%2C_Kerala%2C_India.jpg"
    }
    citySection.innerHTML = `
        <article id="place">
            <img src="${image}" alt="">
            <div>
                <h3>${city}</h3>
                <span>Temperature: ${temperature}&deg;C</span>
                <span>Wind speed: ${windSpeed}mph</span>
            </div>
        </article>
    `
}
